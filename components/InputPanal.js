import React from "react";
import { Card, CardContent, Typography, Grid, TextField, Box } from "@mui/material";

export default function InputPanel(props) {
    return (
        <Card elevation={5} sx={{width: '100%', height: '100%'}}>
            <CardContent sx={{padding: 2}}>
                <Grid container direction="column" spacing={2} sx={{padding: 0}}>
                    <Grid item width='100%' display='flex' justifyContent='center'>
                        <Typography variant="h5">{props.title}</Typography>
                    </Grid>
                    {
                        props.items.map((item) => {
                            return (
                                <Grid item key={item.name}>
                                    <Box style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
                                        <Typography variant="h6" whiteSpace='nowrap'>
                                            {item.name}
                                        </Typography>
                                        {item.input}
                                    </Box>
                                </Grid>
                            )
                        })
                    }
                    <Grid container item justifyContent="flex-end">
                        {props.buttons}
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    )
}