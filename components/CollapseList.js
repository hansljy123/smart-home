import { Collapse, Button, Typography, Paper, Grid } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

export default class CollapseList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isChecked: false
        }
        this.handleCheck = this.handleCheck.bind(this)
    }

    handleCheck() {
        this.setState({
            isChecked: this.state.isChecked ? false : true
        })
    }

    render() {
        return (
            <Paper elevation={2} sx={{minWidth: "95%", padding: 2, margin: 0.5}}>
                <Box sx={{minWidth: "95%", display: "flex", justifyContent: "space-between", alignItems: "center"}}>
                    <Box sx={{display: "inline-block"}}>
                        <Typography variant="h7">{this.props.title}</Typography>
                    </Box>
                    <Box sx={{display: "inline-block", marginRight: 0, marginLeft: "auto"}}>
                        <Button onClick={() => {
                            this.handleCheck()
                            this.state.isChecked || this.props.handleFocus()
                        }} variant="contained">
                            <Typography variant="button">{this.state.isChecked ? '收起': '展开'}</Typography>
                        </Button>
                    </Box>
                </Box>
                <Collapse in={this.state.isChecked}>
                    {this.props.children}
                </Collapse>
            </Paper>
        )
    }
}