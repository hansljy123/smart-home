import { Button, Modal, Box} from "@mui/material";
import React from "react";

export default class ModalWrapper extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
    }

    render() {
        return (
            <>
                {
                    React.cloneElement(this.props.button, {
                        onClick: () => {
                            this.setState({isOpen: true})
                            this.props.handleClick && this.props.handleClick()
                        }
                    })
                }
                <Modal open={this.state.isOpen} onClose={() => this.setState({isOpen: false})}>
                    <>
                        {this.props.children}
                    </>
                </Modal>
            </>
        )
    }
}