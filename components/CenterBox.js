import { Box } from "@mui/system";

export default function CenterBox(props) {
    return (
        <Box position='absolute' top='50%' left='50%' sx={{transform: 'translate(-50%, -50%)'}}>
            {props.children}
        </Box>
    )
}