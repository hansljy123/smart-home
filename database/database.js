import { assert } from 'console'
import mysql from 'mysql2'
import getConfig from 'next/config'
import fs from 'node:fs/promises'

const { serverRuntimeConfig } = getConfig()
const databaseConfig = serverRuntimeConfig.databaseConfig

const connection = mysql.createConnection({
    host: databaseConfig.host,
    user: databaseConfig.user,
    password: databaseConfig.password,
    database: databaseConfig.database
})

export const databaseUtils = {
    users: {
        getUserByName,
        getUserByPhoneNumber,
        insertUser,
        getUserById,
        updateUserById,
        deleteUserById
    },
    space: {
        getSpaceById,
        deleteSpace,
        updateSpace
    },
    room: {
        getRoom,
        deleteRoom,
        updateRoom
    },
    picture: {
        getPicture
    },
    furniture: {
        updateFurniture,
        updateFurniturePosition,
        deleteFurniture
    },
    userSpace: {
        getUserSpaceById,
        createUserSpace
    },
    spaceRoom: {
        getRoomBySpaceId,
        createRoomForSpace
    },
    roomFurniture: {
        getFurnitureByRoomId,
        createFurnitureForRoom
    },
    spaceFurniture: {
        getFurniturePositions
    }
}

function getUserByPhoneNumber(phoneNumber) {
    return connection.promise().execute('select phoneNumber from Users where phoneNumber = ?', [phoneNumber])
    .then(([rows, _]) => {
        return rows;
    })
}

function getPicture(pictureId) {
    return connection.promise().execute('select pictureData from Pictures where pictureId = ?', [pictureId]).then(([rows, _]) => {
        return rows[0].pictureData
    })
}

function getUserByName(name) {
    return connection.promise().execute('select * from Users where userName = ?;', [name]).then(([rows, fields]) => {
        assert(rows.length <= 1)
        return rows[0]
    })
}

function insertUser(user) {
    return connection.promise().execute('insert into Users (userName, phoneNumber, email, hash) values (?, ?, ?, ?)', [
        user.userName,
        user.phoneNumber,
        user.email,
        user.hash
    ]).then(res => {
        return res[0].insertId
    })
}

function getUserById(id) {
    return connection.promise().execute('select * from Users where userId = ?', [id]).then(([rows, fields]) => {
        assert(rows.length <= 1)
        return rows[0]
    })
}

function updateUserById(id, user) {
    let clause = []
    let params = []
    if (user.hasOwnProperty('userName')) {
        clause.push('userName = ?')
        params.push(user.userName)
    }
    if (user.hasOwnProperty('email')) {
        clause.push('email = ?')
        params.push(user.email)
    }
    if (user.hasOwnProperty('hash')) {
        clause.push('hash = ?')
        params.push(user.hash)
    }
    params.push(id)
    return connection.promise().execute(`update Users set ${clause.join(',')} where userId = ?`, params)
}

function deleteUserById(id) {
    // TODO
}


function getSpaceById(spaceId) {
    return connection.promise().execute(`select * from Spaces where spaceId = ?`, [spaceId]).then(([rows, fields]) => {
        return rows[0]
    })
}

function getUserSpaceById(id) {
    return connection.promise().execute(`select * from UserSpaces where userId = ?`, [id]).then(([rows, fields]) => {
        return Promise.all(rows.map(({userId, spaceId}) => {
            return connection.promise().execute(`select * from Spaces where spaceId = ?`, [spaceId]).then(([rows, fields]) => {
                return rows[0]
            })
        }))
    })
}

async function createUserSpace(userId, userSpace) {
    const dest = '/var/lib/mysql-files/tmp'
    await fs.copyFile(userSpace.picture, dest)
    return connection.promise().execute(`select spaceName from UserSpaces natural join Spaces where userId = ?`, [userId])
    .then(([spaces, _]) => {
        if (spaces.find(space => space.spaceName === userSpace.spaceName)) {
            throw `场景名 ${userSpace.spaceName} 已存在`
        } else {
            return connection.promise().execute(`insert into Pictures (pictureData) values (load_file(?))`, [dest])
            .then(pictures => {
                return pictures[0].insertId
            })
            .then(pictureId => {
                return connection.promise().execute(`insert into Spaces (spaceName, description, pictureId) values (?, ?, ?)`, [
                    userSpace.spaceName, userSpace.description, pictureId
                ])
            })
            .then(res => {
                return res[0].insertId
            })
            .then(spaceId => {
                return connection.promise().execute(`insert into UserSpaces (spaceId, userId) values (?, ?)`, [spaceId, userId])
            })
        }
    })
}

function updateSpace(userId, spaceId, space) {
    let prevCheck = space.spaceName
                    ? connection.promise().execute(`select * from UserSpaces natural join Spaces where userId = ? and spaceName = ?`, [userId, space.spaceName])
                      .then(([spaces, _]) => {
                          if (spaces.find(curSpace => curSpace.spaceId != spaceId)) {
                              throw `场景名 ${space.spaceName} 已存在`
                          } else {
                              return Promise.resolve()
                          }
                      })
                    : Promise.resolve()
    return prevCheck.then(() => {
        let clause = [], params = []
        if (space.hasOwnProperty('spaceName')) {
            clause.push('spaceName = ?')
            params.push(space.spaceName)
        }
        if (space.hasOwnProperty('description')) {
            clause.push('description = ?')
            params.push(space.description)
        }
        params.push(spaceId)
        return connection.promise().execute(`update Spaces set ${clause.join(', ')} where spaceId = ?`, params)
    })
}

function getRoomBySpaceId(spaceId) {
    return connection.promise().execute(`select roomId from SpaceRooms where spaceId = ?`, [spaceId])
    .then(([rooms, _]) => {
        return Promise.all(rooms.map(({roomId}) => {
            return connection.promise().execute(`select roomId, roomName, count(furnitureId) as count from Rooms natural left join RoomFurnitures where roomId = ? group by roomId, roomName`, [roomId])
            .then(([roomInfos, _]) => {
                return roomInfos[0]
            })
            
        }))
    })
}

function createRoomForSpace(spaceId, room) {
    return connection.promise().execute(`select roomName from SpaceRooms natural join Rooms where spaceId = ?`, [spaceId])
    .then(([rooms, _]) => {
        if (rooms.find(curRoom => curRoom.roomName === room.roomName)) {
            throw `房间名 ${room.roomName} 已存在`
        } else {
            return connection.promise().execute(`insert into Rooms (roomName) values (?)`, [room.roomName])
        }
    }).then(res => {
        const roomId = res[0].insertId
        return connection.promise().execute(`insert into SpaceRooms (spaceId, roomId) values (?, ?)`, [spaceId, roomId])
    })
}

function getRoom(roomId) {
    return connection.promise().execute(`select * from Rooms where roomId = ?`, [roomId]).then(([rooms, _]) => {
        return rooms[0]
    })
}

function getFurnitureByRoomId(roomId) {
    return connection.promise().execute(`select Furnitures.* from RoomFurnitures natural join Furnitures where roomId = ?`, [roomId]).then(([furnitures, _]) => {
        return furnitures
    })
}

function createFurnitureForRoom(roomId, furniture) {
    return connection.promise().execute(`select furnitureName from RoomFurnitures natural join Furnitures where roomId = ?`, [roomId])
    .then(([furnitures, _]) => {
        if (furnitures.find(curFurniture => curFurniture.furnitureName === furniture.furnitureName)) {
            throw `家具名 ${furniture.furnitureName} 已存在`
        } else {
            return connection.promise().execute(`insert into Furnitures (furnitureName, furnitureType, furnitureStatus, furnitureValue) values (?, ?, ?, ?)`, [
                furniture.furnitureName,
                furniture.furnitureType,
                furniture.furnitureStatus,
                furniture.furnitureValue
            ])
        }
    }).then(res => {
        const furnitureId = res[0].insertId
        return connection.promise().execute(`insert into RoomFurnitures (roomId, furnitureId) values (?, ?)`, [roomId, furnitureId])
    })
}

function updateFurniture(roomId, furnitureId, newFurniture) {
    let prevCheck = newFurniture.furnitureName  
                    ? connection.promise().execute(`select furnitureId from RoomFurnitures natural join Furnitures where roomId = ? and furnitureName = ?`, [roomId, newFurniture.furnitureName])
                      .then(([furnitures, _]) => {
                        if (furnitures.find(curFurniture => curFurniture.furnitureId != furnitureId)) {
                            throw `家具名 ${newFurniture.furnitureName} 已存在`
                        } else {
                            return Promise.resolve()
                        }
                      })
                    : Promise.resolve()
    
    return prevCheck.then(() => {
        let clause = [], params = []
    
        if (newFurniture.hasOwnProperty('furnitureName')) {
            clause.push('furnitureName = ?')
            params.push(newFurniture.furnitureName)
        }
    
        if (newFurniture.hasOwnProperty('furnitureStatus')) {
            clause.push('furnitureStatus = ?')
            params.push(newFurniture.furnitureStatus)
        }
    
        if (newFurniture.hasOwnProperty('furnitureValue')) {
            clause.push('furnitureValue = ?')
            params.push(newFurniture.furnitureValue)
        }
    
        params.push(furnitureId)
        return connection.promise().execute(`update Furnitures set ${clause.join(', ')} where furnitureId = ?`, params)
    })
}

function deleteSpace(spaceId) {
    return connection.promise().execute(`select roomId from SpaceRooms where spaceId = ?`, [spaceId])
    .then(([rooms, _]) => {
        return Promise.all(rooms.map(room => {
            return deleteRoom(room.roomId)
        }))
    })
    .then(() => {
        // TODO: delete picture
        return connection.promise().execute(`delete from Spaces where spaceId = ?`, [spaceId])
    })
}

function deleteRoom(roomId) {
    return connection.promise().execute(`select furnitureId from RoomFurnitures where roomId = ?`, [roomId])
    .then(([furnitures, _]) => {
        return Promise.all(furnitures.map(furniture => {
            return deleteFurniture(furniture.furnitureId)
        }))
    })
    .then(() => {
        return connection.promise().execute(`delete from Rooms where roomId = ?`, [roomId])
    })
}

function updateRoom(spaceId, roomId, room) {
    let prevCheck = room.roomName 
                    ? connection.promise().execute(`select roomId from SpaceRooms natural join Rooms where spaceId = ? and roomName = ?`, [spaceId, room.roomName])
                    .then(([rooms]) => {
                        if (rooms.find(curRoom => curRoom.roomId != roomId)) {
                            throw `房间名 ${room.roomName} 已存在`
                        } else {
                            return Promise.resolve()
                        }
                    })
                    : Promise.resolve()
    return prevCheck.then(() => {
        let clause = [], params = []
        if (room.hasOwnProperty('roomName')) {
            clause.push('roomName = ?')
            params.push(room.roomName)
        }
        params.push(roomId)
        return connection.promise().execute(`update Rooms set ${clause.join(', ')} where roomId = ?`, params)
    })
}

function deleteFurniture(furnitureId) {
    return connection.promise().execute(`delete from Furnitures where furnitureId = ?`, [furnitureId])
}

function getFurniturePositions(spaceId) {
    return connection.promise().execute(`select Furnitures.* from SpaceRooms natural join RoomFurnitures natural join Furnitures where spaceId = ?`, [spaceId])
    .then(([positions, _]) => {
        return positions
    })
}

function updateFurniturePosition(furnitureId, furnitureX, furnitureY) {
    return connection.promise().execute(`update Furnitures set furnitureX = ?, furnitureY = ? where furnitureId = ?`, [furnitureX, furnitureY, furnitureId])
}