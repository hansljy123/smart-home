import { userService } from '../services/user.service'
import getConfig from 'next/config'
import Router from 'next/router';

const { publicRuntimeConfig } = getConfig();

const fetchWrapper = {
    get,
    post,
    postWithFormData,
    put,
    delete: _delete
}

export default fetchWrapper;

function get(url) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            ...authHeader(url)
        }
    }
    return fetch(url, requestOptions).then(handleResponse)
}

function post(url, params) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            ...authHeader(url)
        },
        credentials: 'include',
        body: JSON.stringify(params)
    }
    return fetch(url, requestOptions).then(handleResponse)
}

function postWithFormData(url, formData) {
    const requestOptions = {
        method: 'POST',
        headers: {
            ...authHeader(url)
        },
        credentials: 'include',
        body: formData
    }
    return fetch(url, requestOptions).then(handleResponse)
}

function put(url, body) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            ...authHeader(url)
        },
        body: JSON.stringify(body)
    }
    return fetch(url, requestOptions).then(handleResponse)
}

function _delete(url) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            ...authHeader(url)
        }
    }
    return fetch(url, requestOptions).then(handleResponse)
}

function authHeader(url) {
    const user = userService.localUserValue();
    const isLoggedIn = user && user.token;
    const isApiUrl = url.startsWith(publicRuntimeConfig.apiUrl)
    if (isLoggedIn && isApiUrl) {
        return {
            Authorization: `Bearer ${user.token}`
        }
    } else {
        return {}
    }
}

function handleResponse(response) {
    const status = response.status
    return response.text().then(text => {
        const data = text && JSON.parse(text)

        // TODO: Why log out here

        if (response.status === 401) {
            // Invalid token
            return userService.logout().then(() => {
                alert('会话已过期，请重新登陆')
                return Promise.resolve()
            })
        }

        if (!response.ok) {
            const error = (data && data.message) || response.statusText
            alert(error)
            return Promise.reject(error)
        }
        return data;
    })
}