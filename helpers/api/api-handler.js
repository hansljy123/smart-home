import { errorHandler } from './error-handler';
import { jwtMiddleware } from './jwt-middleware';
import { parse } from 'then-busboy'

export function apiHandler(handler) {
    return async (req, res) => {
        if (!req.headers.hasOwnProperty('content-type') || req.headers['content-type'] === 'application/json') {
            const readable = req.read()
            const buffer = Buffer.from(readable || '{}')
            req.body = JSON.parse(buffer.toString())
        } else {
            req.body = {}
            await parse(req).then(async body => {
                for (const [path, value] of body) {
                    req.body[path[0]] = value.path || value.valueOf()
                }
            })
        } 

        const method = req.method.toLowerCase();
        console.log(`Receive ${method} request to api: ${req.url}`)

        if (!handler[method]) {
            return res.status(405).json({
                message: `Method ${req.method} not allowed`
            })
        }

        try {
            await jwtMiddleware(req, res)
            await handler[method](req, res)
        } catch (err) {
            errorHandler(err, res)
        }
    }
}