export function omit(obj, field) {
    const {[field]: omitted, ...rest} = obj
    return rest
}