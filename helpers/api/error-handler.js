
export function errorHandler(err, res) {
    if (err.name === 'UnauthorizedError') {
        // jwt error
        return res.status(401).json({
            message: 'Invalid token'
        })
    }

    if (typeof(err) == 'string') {
        // customed error
        const is404 = err.toLowerCase().endsWith('not found')
        return res.status(is404 ? 404 : 400).json({
            message: err
        })
    }

    return res.status(500).json({
        message: err.message
    })
}