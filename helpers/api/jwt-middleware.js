import { expressjwt } from "express-jwt";
import getConfig from "next/config";
import util from 'util'

const { serverRuntimeConfig } = getConfig()

// TODO: what does this do?
export function jwtMiddleware(req, res) {
    const middleware = expressjwt({secret: serverRuntimeConfig.encryptionConfig.secret, algorithms: ['HS256']}).unless({
        path: [
            /\/api\/users\/authentication/,
            /\/api\/users\/register/,
            /\/api\/pictures\/*/
        ]
    })
    return util.promisify(middleware)(req, res)
}