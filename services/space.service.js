import fetchWrapper from "../helpers/fetch-wrapper"
import getConfig from "next/config"

const { publicRuntimeConfig } = getConfig()
const spaceURL = `${publicRuntimeConfig.apiUrl}/spaces`
const spaceRoomURL = `${publicRuntimeConfig.apiUrl}/spacerooms`
const spaceFurnitureURl = `${publicRuntimeConfig.apiUrl}/spacefurnitures`

export const spaceService = {
    getSpaceById,
    getSpaceRooms,
    getSpaceFurnitures,
    deleteSpace,
    updateSpace,
    createRoom
}

function getSpaceById(spaceId) {
    return fetchWrapper.get(`${spaceURL}/${spaceId}`)
}

function getSpaceRooms(spaceId) {
    return fetchWrapper.get(`${spaceRoomURL}/${spaceId}`)
}

function getSpaceFurnitures(spaceId) {
    return fetchWrapper.get(`${spaceFurnitureURl}/${spaceId}`)
}

function deleteSpace(spaceId) {
    return fetchWrapper.delete(`${spaceURL}/${spaceId}`)
}

function updateSpace(spaceId, space) {
    return fetchWrapper.put(`${spaceURL}/${spaceId}`, space)
}

function createRoom(spaceId, room) {
    return fetchWrapper.post(`${spaceRoomURL}/${spaceId}`, room)
}