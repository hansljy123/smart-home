import fetchWrapper from "../helpers/fetch-wrapper"
import getConfig from "next/config"

const { publicRuntimeConfig } = getConfig()
const furnituresURL = `${publicRuntimeConfig.apiUrl}/furnitures`
const furniturePositionURL = `${publicRuntimeConfig.apiUrl}/furnitureposition`

export const furnitureService = {
    updateFurniture,
    updateFurniturePosition,
    deleteFurniture
}

function updateFurniturePosition(furnitureId, furnitureX, furnitureY) {
    return fetchWrapper.put(`${furniturePositionURL}/${furnitureId}`, {
        furnitureX, furnitureY
    })
}

function updateFurniture(roomId, furnitureId, furniture) {
    return fetchWrapper.put(`${furnituresURL}/${furnitureId}`, {
        roomId,
        ...furniture
    })
}

function deleteFurniture(furnitureId) {
    return fetchWrapper.delete(`${furnituresURL}/${furnitureId}`)
}