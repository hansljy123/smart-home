import fetchWrapper from "../helpers/fetch-wrapper";
import getConfig from "next/config";
import Router, { useRouter } from "next/router";
import { BehaviorSubject } from 'rxjs'

const { publicRuntimeConfig } = getConfig();
const baseURL = `${publicRuntimeConfig.apiUrl}/users`

export const userService = {
    localUserValue,
    login,
    logout,
    register,
    getById,
    update,
    delete: _delete
}

function localUserValue() {
    return JSON.parse(localStorage.getItem('user'))
}

function login(userName, password) {
    return fetchWrapper.post(`${baseURL}/authentication`, {userName, password}).then(
        user => {
            if (typeof window !== 'undefined') {
                localStorage.setItem('user', JSON.stringify(user))
            }
            return user
        }
    )
}

function logout() {
    if (typeof window !== 'undefined') {
        localStorage.removeItem('user')
    }
    return Router.push('/').then(() => {
        return Router.reload(window.location.pathname)
    })
}

function register(user) {
    return fetchWrapper.post(`${baseURL}/register`, user)
}

function getById(userid) {
    return fetchWrapper.get(`${baseURL}/${userid}`)
}

function update(userid, params) {
    return fetchWrapper.put(`${baseURL}/${userid}`, params).then(
        x => {
            if (userid == userSubject.value.id) {
                const newUser = {
                    ...userSubject.value,
                    ...params
                }
                if (typeof window !== 'undefined') {
                    localStorage.setItem('user', JSON.stringify(newUser))
                }
            }
        }
    )
}

function _delete(userid) {
    return fetchWrapper.delete(`${baseURL}/${userid}`)
}

