import fetchWrapper from "../helpers/fetch-wrapper"
import getConfig from "next/config"

const { publicRuntimeConfig } = getConfig()
const roomsURL = `${publicRuntimeConfig.apiUrl}/rooms`
const roomFurnituresURL = `${publicRuntimeConfig.apiUrl}/roomfurnitures`

export const roomServices = {
    getRoom,
    getRoomFurnitures,
    deleteRoom,
    updateRoom,
    createFurniture
}


function getRoom(roomId) {
    return fetchWrapper.get(`${roomsURL}/${roomId}`)
}

function getRoomFurnitures(roomId) {
    return fetchWrapper.get(`${roomFurnituresURL}/${roomId}`)
}

function deleteRoom(roomId) {
    return fetchWrapper.delete(`${roomsURL}/${roomId}`)
}

function updateRoom(spaceId, roomId, room) {
    return fetchWrapper.put(`${roomsURL}/${roomId}`, {
        spaceId,
        ...room
    })
}

function createFurniture(roomId, furniture) {
    return fetchWrapper.post(`${roomFurnituresURL}/${roomId}`, furniture)
}