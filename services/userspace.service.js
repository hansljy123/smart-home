import fetchWrapper from "../helpers/fetch-wrapper"
import getConfig from "next/config"

const { publicRuntimeConfig } = getConfig()
const baseURL = `${publicRuntimeConfig.apiUrl}/userspaces`

export const userSpaceService = {
    getUserAllSpaces,
    createUserSpace
}

function getUserAllSpaces(userId) {
    return fetchWrapper.get(`${baseURL}/${userId}`)
}

function createUserSpace(userId, newSpace) {
    return fetchWrapper.postWithFormData(`${baseURL}/${userId}`, newSpace)
}