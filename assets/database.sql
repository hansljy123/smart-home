drop database SmartHome;
create database SmartHome;
use SmartHome;

create table Users (
    userId int auto_increment key,
    userName varchar(40) unique,
    phoneNumber char(13),
    email char(40),
    hash varchar(60)
);

create table Pictures (
    pictureId int auto_increment key,
    pictureData mediumblob
);

create table Spaces (
    spaceId int auto_increment key,
    spaceName varchar(40),
    description varchar(200),
    pictureId int
);


create table Rooms (
    roomId int auto_increment key,
    roomName varchar(40)
);

create table Furnitures (
    furnitureId int auto_increment key,
    furnitureName varchar(40),
    furnitureType enum('light', 'lock', 'sensor', 'switch'),
    furnitureStatus boolean,
    furnitureValue int,
    furnitureX int default 0,
    furnitureY int default 0
);

create table UserSpaces (
    userId int,
    spaceId int,
    foreign key (userId) references Users(userId)
        on update cascade on delete cascade,
    foreign key (spaceId) references Spaces(spaceId)
        on update cascade on delete cascade
);

create table SpaceRooms (
    spaceId int,
    roomId int,
    foreign key (spaceId) references Spaces(spaceId)
        on update cascade on delete cascade,
    foreign key (roomId) references Rooms(roomId)
        on update cascade on delete cascade
);

create table RoomFurnitures (
    roomId int,
    furnitureId int,
    foreign key (roomId) references Rooms(roomId)
        on update cascade on delete cascade,
    foreign key (furnitureId) references Furnitures(furnitureId)
        on update cascade on delete cascade
);