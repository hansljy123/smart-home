import { Box, Button, Grid, Typography, TextField, IconButton, Paper, Table, TableContainer, TableHead, TableCell, TableBody, TableRow, fabClasses } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import AddIcon from '@mui/icons-material/Add';
import Layout from "../../components/Layout"
import React, { useEffect, useState } from "react";
import {default as NextLink} from "next/link";
import { Link } from "@mui/material";
import { useRouter } from "next/router";
import { spaceService } from "../../services/space.service";
import ModalWrapper from '../../components/ModalWrapper'
import InputPanel from "../../components/InputPanal";
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { roomServices } from "../../services/rooms.service";
import CenterBox from "../../components/CenterBox";
import Image from "next/image";
import { fabric } from "fabric";
import { FabricJSCanvas, useFabricJSEditor } from "fabricjs-react";
import { promisify } from "util";
import CheckIcon from '@mui/icons-material/Check';
import { furnitureService } from '../../services/furnitures.service'
import * as yup from 'yup'

const getImageFromURL = promisify(fabric.Image.fromURL)

export default function Space(props) {

    const router = useRouter()
    const [isLoaded, setLoaded] = useState(false)
    const [isCanvasReady, setCanvasReady] = useState(false)
    const [rooms, setRooms] = useState([])
    const [space, setSpace] = useState({})

    const [newRoomName, setNewRoomName] = useState('')
    const [updateRoomName, setUpdateRoomName] = useState('')

    const [furnitures, setFurnitures] = useState([])
    const [furnitureObjs, setFurnitureObjs] = useState([])

    const schema = yup.object().shape({
        roomName: yup.string().required('请输入房间名')
    })

    const { editor, onReady } = useFabricJSEditor()

    useEffect(() => {
        if (isLoaded && isCanvasReady) {
            let newFurnitureObjs = new Array(furnitures.length)
            fabric.Image.fromURL(`/api/pictures/${space.pictureId}`, async oImg => {
                editor.canvas.clear()

                editor.canvas.setBackgroundImage(oImg)
                editor.canvas.setWidth(oImg.width)
                editor.canvas.setHeight(oImg.height)

                furnitures.map(({furnitureType, furnitureX, furnitureY}, id) => {
                    fabric.Image.fromURL(`/imgs/${furnitureType}.png`, icon => {
                        newFurnitureObjs[id] = icon
                        editor.canvas.add(icon)
                    }, {
                        width: 24,
                        height: 24,
                        hasControls: false,
                        //cornerColor: 'green',cornerSize: 16,transparentCorners: false,
                        selection: false,       
                        lockRotation:false,
                        //lockMovement: false,lockMovementY: false,lockMovementX: false,
                        //lockUniScaling: false,lockScalingY:false, lockScalingX:false,
                        hoverCursor: 'default',
                        hasRotatingPoint: false,
                        angle: 0,
                        cornersize: 10,
                        left: furnitureX, 
                        top: furnitureY,
                    })
                })
            })
            setFurnitureObjs(newFurnitureObjs)
        }
    }, [isLoaded, furnitures])

    useEffect(() => {
        if (editor && fabric) {
            setCanvasReady(true)
        }
    }, [editor, fabric])

    useEffect(() => {
        if (router.isReady) {
            setLoaded(false)
            const spaceId = router.query.id
            spaceService.getSpaceById(spaceId)
            .then(space => {
                setSpace(space)
                return Promise.resolve()
            })
            .then(() => {
                return spaceService.getSpaceRooms(spaceId)
            })
            .then(rooms => {
                setRooms(rooms)
                return Promise.resolve()
            })
            .then(() => {
                return spaceService.getSpaceFurnitures(spaceId)
            })
            .then((furnitures) => {
                setFurnitures(furnitures)
                return Promise.resolve()
            })
            .then(() => {
                setLoaded(true)
            })
        }
    }, [router.isReady])

    async function handleNewRoom() {
        const spaceId = router.query.id

        let valid = true
        try {
            await schema.validate({
                roomName: newRoomName
            })
        } catch(err) {
            alert(err.errors[0])
            valid = false;
        }

        if (valid) {
            return spaceService.createRoom(spaceId, {
                roomName: newRoomName
            })
        } else {
            return Promise.resolve()
        }

    }

    async function handleUpdateRoom(roomId) {
        const spaceId = router.query.id

        let valid = true
        try {
            await schema.validate({
                roomName: updateRoomName
            })
        } catch(err) {
            alert(err.errors[0])
            valid = false;
        }

        if (valid) {
            return roomServices.updateRoom(spaceId, roomId, {
                roomName: updateRoomName
            })
        } else {
            return Promise.resolve()
        }
    }

    function handleDeleteRoom(roomId) {
        return roomServices.deleteRoom(roomId)
    }

    function handleUpdatePosition() {
        return Promise.all(furnitureObjs.map((furnitureObj, id) => {
            const {x, y} = furnitureObj.getPointByOrigin()
            return furnitureService.updateFurniturePosition(furnitures[id].furnitureId, x, y)
        }))
    }

    function refresh() {
        const spaceId = router.query.id
        return spaceService.getSpaceById(spaceId)
        .then(space => {
            setSpace(space)
            return Promise.resolve()
        })
        .then(() => {
            return spaceService.getSpaceRooms(spaceId)
        })
        .then(rooms => {
            setRooms(rooms)
            return Promise.resolve()
        })
        .then(() => {
            return spaceService.getSpaceFurnitures(spaceId)
        })
        .then((furnitures) => {
            setFurnitures(furnitures)
            return Promise.resolve()
        })
    }

    return (
        <Layout>
            <Grid container direction="row" spacing={0}>
                <Grid item display={'flex'} alignItems='center'>
                    <Typography display={'inline-block'} variant="h3" marginY={2} align="center">{space.spaceName}</Typography>
                    <Button variant="contained" sx={{marginLeft: 2}} onClick={handleUpdatePosition}>提交家具位置</Button>
                </Grid>
            </Grid>
            <Box width='100%' id="canvas-box" marginBottom={5}>
                <FabricJSCanvas onReady={onReady}></FabricJSCanvas>
            </Box>
            <Grid container>
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    <Box sx={{display: 'flex', alignItems: 'flex-end'}}>
                                        <Typography variant="h6" display="inline">房间</Typography>
                                        <ModalWrapper button={
                                            <IconButton><AddIcon></AddIcon></IconButton>
                                        } handleClick={() => {
                                            setNewRoomName('')
                                        }}>
                                            <CenterBox>
                                                <InputPanel title='新建房间' items={[
                                                    {
                                                        name: '房间名',
                                                        input: (
                                                            <TextField variant="outlined" defaultValue='' onChange={(event) => {
                                                                setNewRoomName(event.target.value)
                                                            }} sx={{marginLeft: 2}} fullWidth>
                                                            </TextField>
                                                        )
                                                    }
                                                ]} buttons={<Button onClick={async () => {
                                                    await handleNewRoom()
                                                    await refresh()
                                                }}>提交</Button>}></InputPanel>
                                            </CenterBox>
                                        </ModalWrapper>
                                    </Box>
                                </TableCell>
                                <TableCell align='right'>
                                    <Typography variant="h6">家具数量</Typography>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                rooms.map(({roomId, roomName, count}) => {
                                    return (
                                        <TableRow
                                            key={roomId}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell>
                                                <Box>
                                                    <Link component={NextLink} href={`/rooms/${roomId}`}>
                                                        {roomName}
                                                    </Link>
                                                    <ModalWrapper button={
                                                        <IconButton sx={{marginLeft: 1}}><EditIcon></EditIcon></IconButton>
                                                    } handleClick={() => {
                                                        setUpdateRoomName(roomName)
                                                    }}>
                                                        <CenterBox>
                                                            <InputPanel title='修改房间信息' items={[
                                                                {
                                                                    name: '房间名',
                                                                    input: (
                                                                        <TextField variant="outlined" defaultValue={roomName} onChange={(event) => {
                                                                            setUpdateRoomName(event.target.value)
                                                                        }} sx={{marginLeft: 2}} fullWidth>
                                                                        </TextField>
                                                                    )
                                                                }
                                                            ]} buttons={
                                                                <Button onClick={async () => {
                                                                    await handleUpdateRoom(roomId)
                                                                    await refresh()
                                                                }}>提交</Button>
                                                            }/>
                                                        </CenterBox>
                                                    </ModalWrapper>
                                                    <IconButton onClick={async () => {
                                                        await handleDeleteRoom(roomId)
                                                        await refresh()
                                                    }}><DeleteForeverIcon></DeleteForeverIcon></IconButton>
                                                </Box>
                                            </TableCell>
                                            <TableCell align="right">{count}</TableCell>
                                        </TableRow>
                                    )
                                })
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </Layout>

    )
}