import { Button, Card, CardContent, Grid, TextField, Typography } from "@mui/material";
import React from "react";
import Layout from "../../components/Layout";

export default class UserInfo extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Layout isLoggedIn={this.props.isLoggedIn} userName={this.props.userName}>
                <Typography variant="h3" marginLeft={5} marginY={2}>用户信息</Typography>
                <Card elevation={5} sx={{maxWidth: "400px", marginLeft: 5, marginY: 2}}>
                    <CardContent>
                        <Grid container direction="column" rowSpacing={2}>
                            <Grid container item alignItems="flex-end" columnSpacing={2}>
                                <Grid item xs={4}>
                                    <Typography align="right"> 用户名 </Typography>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField id="username" variant="standard"></TextField>
                                </Grid>
                            </Grid>
                            <Grid container item alignItems="flex-end" columnSpacing={2}>
                                <Grid item xs={4}>
                                    <Typography align="right"> 邮箱 </Typography>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField id="email" variant="standard"></TextField>
                                </Grid>
                            </Grid>
                            <Grid container item alignItems="flex-end" columnSpacing={2}>
                                <Grid item xs={4}>
                                    <Typography align="right"> 手机号码 </Typography>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField id="email" variant="standard"></TextField>
                                </Grid>
                            </Grid>
                            <Grid container item alignItems="flex-end" columnSpacing={2}>
                                <Grid item xs={4}>
                                    <Typography align="right"> 密码 </Typography>
                                </Grid>
                                <Grid item xs={8}>
                                    <TextField id="email" variant="standard" type="password"></TextField>
                                </Grid>
                            </Grid>
                            <Grid container item justifyContent="flex-end">
                                <Button>
                                    <Typography>
                                        修改
                                    </Typography>
                                </Button>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Layout>
        )
    }
}