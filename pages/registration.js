import InputPanel from "../components/InputPanal"
import Layout from "../components/Layout"
import CenterBox from "../components/CenterBox"
import { Button, TextField } from "@mui/material"
import { useState } from "react"
import * as yup from 'yup'
import { userService } from "../services/user.service"
import { useRouter } from "next/router"

export default function Registration() {
    const router = useRouter()

    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const [passwordRepeat, setPasswordRepeat] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [email, setEmail] = useState('')

    // phone number validation
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    const schema = yup.object().shape({
        userName: yup.string().required('请输入用户名').min(6, '用户名至少 6 个字符'),
        password: yup.string().required('请输入密码').min(6, '密码至少 6 个字符'),
        passwordRepeat: yup.string().oneOf([yup.ref('password')], '两次输入密码不一致'),
        phoneNumber: yup.string().matches(phoneRegExp, '请输入合法的手机号'),
        email: yup.string().email('请输入合法的邮件地址')
    })

    async function handleRegister() {
        let registerInfo = {
            userName, password, passwordRepeat, phoneNumber, email
        }

        let valid = true

        try {
            await schema.validate(registerInfo)
        } catch (err) {
            alert(err.errors[0])
            valid=false
        }
        if (valid) {
            return userService.register({
                userName, password, phoneNumber, email
            }).then(() => {
                router.push('/login')
            })
        } else {
            return Promise.resolve()
        }
    }

    return (
        <Layout>
            <CenterBox>
                <InputPanel title="用户注册" items={[
                    {
                        name: '用户名',
                        input: (
                            <TextField variant="outlined" defaultValue='' onChange={event => {
                                setUserName(event.target.value)
                            }} sx={{marginLeft: 2}} fullWidth></TextField>
                        )
                    }, {
                        name: '密码',
                        input: (
                            <TextField variant="outlined" type='password' defaultValue='' onChange={event => {
                                setPassword(event.target.value)
                            }} sx={{marginLeft: 2}} fullWidth></TextField>
                        )
                    }, {
                        name: '确认密码',
                        input: (
                            <TextField variant="outlined" type='password' defaultValue='' onChange={event => {
                                setPasswordRepeat(event.target.value)
                            }} sx={{marginLeft: 2}} fullWidth></TextField>
                        )
                    }, {
                        name: '邮箱',
                        input: (
                            <TextField variant="outlined" defaultValue='' onChange={event => {
                                setEmail(event.target.value)
                            }} sx={{marginLeft: 2}} fullWidth></TextField>
                        )
                    }, {
                        name: '电话号码',
                        input: (
                            <TextField variant="outlined" defaultValue='' onChange={event => {
                                setPhoneNumber(event.target.value)
                            }} sx={{marginLeft: 2}} fullWidth></TextField>
                        )
                    }
                ]} buttons={
                    <Button variant="contained" onClick={handleRegister}>提交</Button>
                }>
                </InputPanel>
            </CenterBox>
        </Layout>
    )
}