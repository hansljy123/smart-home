import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import { Box, Card, Grid, Typography, Paper, CardContent, Slider, Switch, Button, IconButton, TextField } from "@mui/material";
import CollapseList from "../../components/CollapseList";
import { useRouter } from "next/router";
import { RoomService } from "@mui/icons-material";
import { roomServices } from "../../services/rooms.service";
import AddIcon from '@mui/icons-material/Add';
import ModalWrapper from '../../components/ModalWrapper'
import CenterBox from '../../components/CenterBox'
import InputPanal from '../../components/InputPanal'
import { furnitureService } from '../../services/furnitures.service'
import { boolean } from "yup";

export default function Room(props) {
    const router = useRouter()

    const [lights, setLights] = useState([])
    const [locks, setLocks] = useState([])
    const [sensors, setSensors] = useState([])
    const [switchs, setSwitches] = useState([])
    const [room, setRoom] = useState({})

    const [newName, setNewName] = useState('')
    const [newValue, setNewValue] = useState(0)
    const [newStatus, setNewStatus] = useState(false)

    const [updateValue, setUpdateValue] = useState(0)
    const [updateStatus, setUpdateStatus] = useState(false)

    useEffect(() => {
        const roomId = router.query.id
        roomServices.getRoomFurnitures(roomId)
        .then(res => {
            setLights(res.filter(furniture => furniture.furnitureType === 'light'))
            setLocks(res.filter(furniture => furniture.furnitureType === 'lock'))
            setSensors(res.filter(furniture => furniture.furnitureType === 'sensor'))
            setSwitches(res.filter(furniture => furniture.furnitureType === 'switch'))
            return Promise.resolve()
        })
        .then(() => {
            return roomServices.getRoom(roomId)
        })
        .then(room => {
            setRoom(room)
            return Promise.resolve()
        })

    }, [router.isReady])

    function handleNewFurniture(furnitureType) {
        const roomId = router.query.id
        if (newName === '') {
            alert('请输入家具名')
            return Promise.resolve()
        } else {
            return roomServices.createFurniture(roomId, {
                furnitureType,
                furnitureName: newName,
                furnitureValue: newValue,
                furnitureStatus: newStatus
            })
        }
    }

    function handleUpdateFurniture(furnitureId) {
        const roomId = router.query.id
        return furnitureService.updateFurniture(roomId, furnitureId, {
            furnitureStatus: updateStatus,
            furnitureValue: updateValue
        })
    }

    function handleDeleteFurniture(furnitureId) {
        return furnitureService.deleteFurniture(furnitureId)
    }

    function refresh() {
        const roomId = router.query.id
        return roomServices.getRoomFurnitures(roomId)
        .then(res => {
            setLights(res.filter(furniture => furniture.furnitureType === 'light'))
            setLocks(res.filter(furniture => furniture.furnitureType === 'lock'))
            setSensors(res.filter(furniture => furniture.furnitureType === 'sensor'))
            setSwitches(res.filter(furniture => furniture.furnitureType === 'switch'))
            return Promise.resolve()
        })
        .then(() => {
            return roomServices.getRoom(roomId)
        })
        .then(room => {
            setRoom(room)
            return Promise.resolve()
        })
    }

    return (
        <Layout>
            <Grid container direction="row" spacing={0}>
                <Typography variant="h3" marginY={2} align="center">{room.roomName}</Typography>  
            </Grid>
            <Grid container justifyContent="center" direction="column" spacing={2}>
                <Grid item>
                    <Card elevation={5} sx={{minWidth: "95%"}}>
                        <CardContent>
                            <Box display='flex' alignItems='center'>
                                <Typography variant="h5">灯具</Typography>
                                <ModalWrapper button={
                                    <IconButton><AddIcon></AddIcon></IconButton>
                                }>
                                    <CenterBox>
                                        <InputPanal title='新建灯具' items={[
                                            {
                                                name: '灯具名',
                                                input: (
                                                    <TextField variant="outlined" defaultValue='' onChange={(event) => {
                                                        setNewName(event.target.value)
                                                    }} sx={{marginLeft: 2}}></TextField>
                                                )
                                            }, {
                                                name: '亮度',
                                                input: (
                                                    <Slider sx={{marginLeft: 2}} valueLabelDisplay='auto' onChange={(event) => {
                                                        setNewValue(event.target.value)
                                                    }}></Slider>
                                                )
                                            }, {
                                                name: '开关状态',
                                                input: (
                                                    <Switch sx={{marginLeft: 2}} onClick={(event) => {
                                                        setNewStatus(event.target.checked)
                                                    }}></Switch>
                                                )
                                            }
                                        ]} buttons={
                                            <Button onClick={async () => {
                                                await handleNewFurniture('light')
                                                await refresh()
                                            }}>提交</Button>
                                        }>
                                        </InputPanal>
                                    </CenterBox>
                                </ModalWrapper>
                            </Box>
                            {
                                lights.map(({furnitureId, furnitureName, furnitureStatus, furnitureValue}) => {
                                    return (
                                        <CollapseList key={furnitureId} title={furnitureName} handleFocus={() => {
                                            setUpdateStatus(furnitureStatus)
                                            setUpdateValue(furnitureValue)
                                        }}>
                                            <Box display="flex" alignItems="center" paddingY="5px">
                                                <Switch defaultChecked={Boolean(furnitureStatus)} onChange={event => {
                                                    setUpdateStatus(event.target.checked)
                                                }}></Switch>
                                                <Slider defaultValue={furnitureValue} valueLabelDisplay="auto" onChange={event => {
                                                    setUpdateValue(event.target.value)
                                                }}></Slider>
                                                <Button onClick={async () => {
                                                    await handleUpdateFurniture(furnitureId)
                                                    await refresh()
                                                }}>
                                                    <Typography variant="button">提交</Typography>
                                                </Button>
                                                <Button color="warning" onClick={async () => {
                                                    await handleDeleteFurniture(furnitureId)
                                                    await refresh()
                                                }}>删除</Button>
                                            </Box>
                                        </CollapseList>
                                    )
                                })
                            }
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item>
                    <Card elevation={5} sx={{minWidth: "95%"}}>
                        <CardContent>
                            <Box display='flex' alignItems='center'>
                                <Typography variant="h5">传感器</Typography>
                                <ModalWrapper button={
                                    <IconButton><AddIcon></AddIcon></IconButton>
                                }>
                                    <CenterBox>
                                        <InputPanal title='新建传感器' items={[
                                            {
                                                name: '传感器名称',
                                                input: (
                                                    <TextField variant="outlined" defaultValue='' onChange={(event) => {
                                                        setNewName(event.target.value)
                                                    }} sx={{marginLeft: 2}}></TextField>
                                                )
                                            }, {
                                                name: '开关状态',
                                                input: (
                                                    <Switch defaultChecked={false} onChange={(event) => {
                                                        setNewStatus(event.target.checked)
                                                    }} />
                                                )
                                            }
                                        ]} buttons={
                                            <Button onClick={async () => {
                                                await handleNewFurniture('sensor')
                                                await refresh()
                                            }}>提交</Button>
                                        }>
                                        </InputPanal>
                                    </CenterBox>
                                </ModalWrapper>
                            </Box>
                            {
                                sensors.map(({furnitureId, furnitureName, furnitureStatus, furnitureValue}) => {
                                    return (
                                        <CollapseList key={furnitureId} title={furnitureName} handleFocus={() => {
                                            setUpdateStatus(furnitureStatus)
                                            setUpdateValue(furnitureValue)
                                        }}>
                                            <Box display="flex" alignItems="center" paddingY="5px" justifyContent='space-between'>
                                                <Box>
                                                    <Typography display='inline'>{`示数: ${furnitureValue}`}</Typography>
                                                    <Switch defaultChecked={Boolean(furnitureStatus)} onChange={event => {
                                                        setUpdateStatus(event.target.checked)
                                                    }}></Switch>
                                                </Box>
                                                <Box>
                                                    <Button onClick={async () => {
                                                        await handleUpdateFurniture(furnitureId)
                                                        await refresh()
                                                    }}>
                                                        <Typography variant="button">提交</Typography>
                                                    </Button>
                                                    <Button color="warning" onClick={async () => {
                                                        await handleDeleteFurniture(furnitureId)
                                                        await refresh()
                                                    }}>删除</Button>
                                                </Box>
                                            </Box>
                                        </CollapseList>
                                    )
                                })
                            }
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item>
                    <Card elevation={5} sx={{minWidth: "95%"}}>
                        <CardContent>
                            <Box display='flex' alignItems='center'>
                                <Typography variant="h5">门锁</Typography>
                                <ModalWrapper button={
                                    <IconButton><AddIcon></AddIcon></IconButton>
                                }>
                                    <CenterBox>
                                        <InputPanal title='新建门锁' items={[
                                            {
                                                name: '门锁名称',
                                                input: (
                                                    <TextField variant="outlined" defaultValue='' onChange={(event) => {
                                                        setNewName(event.target.value)
                                                    }} sx={{marginLeft: 2}}></TextField>
                                                )
                                            }, {
                                                name: '门锁状态',
                                                input: (
                                                    <Switch defaultChecked={false} onChange={(event) => {
                                                        setNewStatus(event.target.checked)
                                                    }} />
                                                )
                                            }
                                        ]} buttons={
                                            <Button onClick={async () => {
                                                await handleNewFurniture('lock')
                                                await refresh()
                                            }}>提交</Button>
                                        }>
                                        </InputPanal>
                                    </CenterBox>
                                </ModalWrapper>
                            </Box>
                            {
                                locks.map(({furnitureId, furnitureName, furnitureStatus}) => {
                                    return (
                                        <CollapseList key={furnitureId} title={furnitureName} handleFocus={() => {
                                            setUpdateStatus(furnitureStatus)
                                            setUpdateValue(0)
                                        }}>
                                            <Box display="flex" alignItems="center" paddingY="5px" justifyContent='space-between'>
                                                <Box>
                                                    <Switch defaultChecked={Boolean(furnitureStatus)} onChange={event => {
                                                        setUpdateStatus(event.target.checked)
                                                    }}></Switch>
                                                </Box>
                                                <Box>
                                                    <Button onClick={async () => {
                                                        await handleUpdateFurniture(furnitureId)
                                                        await refresh()
                                                    }}>
                                                        <Typography variant="button">提交</Typography>
                                                    </Button>
                                                    <Button color="warning" onClick={async () => {
                                                        await handleDeleteFurniture(furnitureId)
                                                        await refresh()
                                                    }}>删除</Button>
                                                </Box>
                                            </Box>
                                        </CollapseList>
                                    )
                                })
                            }
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item>
                    <Card elevation={5} sx={{minWidth: "95%"}}>
                        <CardContent>
                            <Box display='flex' alignItems='center'>
                                <Typography variant="h5">开关</Typography>
                                <ModalWrapper button={
                                    <IconButton><AddIcon></AddIcon></IconButton>
                                }>
                                    <CenterBox>
                                        <InputPanal title='新建开关' items={[
                                            {
                                                name: '开关名称',
                                                input: (
                                                    <TextField variant="outlined" defaultValue='' onChange={(event) => {
                                                        setNewName(event.target.value)
                                                    }} sx={{marginLeft: 2}}></TextField>
                                                )
                                            }, {
                                                name: '开关状态',
                                                input: (
                                                    <Switch defaultChecked={false} onChange={(event) => {
                                                        setNewStatus(event.target.checked)
                                                    }} />
                                                )
                                            }
                                        ]} buttons={
                                            <Button onClick={async () => {
                                                await handleNewFurniture('switch')
                                                await refresh()
                                            }}>提交</Button>
                                        }>
                                        </InputPanal>
                                    </CenterBox>
                                </ModalWrapper>
                            </Box>
                            {
                                switchs.map(({furnitureId, furnitureName, furnitureStatus}) => {
                                    return (
                                        <CollapseList key={furnitureId} title={furnitureName} handleFocus={() => {
                                            setUpdateStatus(furnitureStatus)
                                            setUpdateValue(0)
                                        }}>
                                            <Box display="flex" alignItems="center" paddingY="5px" justifyContent='space-between'>
                                                <Box>
                                                    <Switch defaultChecked={Boolean(furnitureStatus)} onChange={event => {
                                                        setUpdateStatus(event.target.checked)
                                                    }}></Switch>
                                                </Box>
                                                <Box>
                                                    <Button onClick={async () => {
                                                        await handleUpdateFurniture(furnitureId)
                                                        await refresh()
                                                    }}>
                                                        <Typography variant="button">提交</Typography>
                                                    </Button>
                                                    <Button color="warning" onClick={async () => {
                                                        await handleDeleteFurniture(furnitureId)
                                                        await refresh()
                                                    }}>删除</Button>
                                                </Box>
                                            </Box>
                                        </CollapseList>
                                    )
                                })
                            }
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </Layout>
    )
}