import Layout from '../components/Layout'
import { Box } from '@mui/system'
import { Link, Typography } from '@mui/material'
import {default as NextLink} from 'next/link'
import { useEffect, useState } from 'react'
import { userService } from '../services/user.service'

export default function Home() {

  const [user, setUser] = useState({})
  
  useEffect(() => {
    const localUser = userService.localUserValue()
    if (localUser) {
      setUser(localUser)
    }
  }, [])

  return (
    <Layout>
      <Box position='absolute' top='40%' left='50%' sx={{transform: 'translate(-50%, -50%)'}}>
        <Typography variant='h1' style={{textAlign: 'center', margin: 0}}>智能家居管理系统</Typography>
        <Box display='flex' justifyContent='center'>
          {
            user.userId && (
              <Link component={NextLink} href={`/userspaces/${user.userId}`}><Typography variant='h4'>进入系统</Typography></Link>
            )
          }
        </Box>
      </Box>
    </Layout>
  )
}
