import '../styles/globals.css'
import React, { use } from 'react';
import { userService } from '../services/user.service'

class MyApp extends React.Component {
  render() {
    return (
      <this.props.Component {...this.props.pageProps} />
    )
  }
}

export default MyApp
