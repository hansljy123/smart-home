import { databaseUtils } from "../../../database/database"
import { apiHandler } from "../../../helpers/api/api-handler"

const furnitureUtils = databaseUtils.furniture

export const config = {
    api: {
        bodyParser: false
    }
}

export default apiHandler({
    put: updateFurniture,
    delete: deleteFurniture
})

function updateFurniture(req, res) {
    const furnitureId = req.query.id
    const { roomId, ...furniture} = req.body

    return furnitureUtils.updateFurniture(roomId, furnitureId, furniture).then(() => {
        res.status(200).json({})
    })
}

function deleteFurniture(req, res) {
    const furnitureId = req.query.id

    return furnitureUtils.deleteFurniture(furnitureId).then(() => {
        res.status(200).json({})
    })
}