import { apiHandler } from "../../../helpers/api/api-handler"
import { databaseUtils } from "../../../database/database"

export const config = {
    api: {
        bodyParser: false
    }
}

export default apiHandler({
    put: updateFurniturePosition
})

function updateFurniturePosition(req, res) {
    const furnitureId = req.query.id
    const {furnitureX, furnitureY} = req.body

    return databaseUtils.furniture.updateFurniturePosition(furnitureId, furnitureX, furnitureY)
    .then(() => {
        return res.status(200).json({})
    })
}