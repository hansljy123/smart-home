import { apiHandler } from "../../../helpers/api/api-handler"
import { databaseUtils } from "../../../database/database"

const spaceUtils = databaseUtils.space

export const config = {
    api: {
        bodyParser: false
    }
}

export default apiHandler({
    get: getSpaceById,
    put: updateSpace,
    delete: deleteSpace
})

function getSpaceById(req, res) {
    const spaceId = req.query.id
    return spaceUtils.getSpaceById(spaceId).then(space => {
        return res.status(200).json(space || [])
    })
}

function deleteSpace(req, res) {
    const spaceId = req.query.id
    return spaceUtils.deleteSpace(spaceId).then(() => {
        return res.status(200).json({})
    })
}

function updateSpace(req, res) {
    const spaceId = req.query.id
    const { userId, ...space } = req.body
    return spaceUtils.updateSpace(userId, spaceId, space).then(() => {
        return res.status(200).json({})
    })
}