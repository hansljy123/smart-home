import { apiHandler } from "../../../helpers/api/api-handler"
import { databaseUtils } from "../../../database/database"

const spaceRoomUtils = databaseUtils.spaceRoom

export const config = {
    api: {
        bodyParser: false
    }
}

export default apiHandler({
    get: getSpaceAllRooms,
    post: createRoomForSpace
})

function getSpaceAllRooms(req, res) {
    const spaceId = req.query.id

    return spaceRoomUtils.getRoomBySpaceId(spaceId).then(rooms => {
        return res.status(200).json(rooms)
    })
}

function createRoomForSpace(req, res) {
    const spaceId = req.query.id
    const room = req.body
    return spaceRoomUtils.createRoomForSpace(spaceId, room).then(rooms => {
        return res.status(200).json({})
    })
}