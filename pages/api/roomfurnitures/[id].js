import { apiHandler } from "../../../helpers/api/api-handler";
import { databaseUtils } from "../../../database/database";

const roomFurnitureUtils = databaseUtils.roomFurniture

export const config = {
    api: {
        bodyParser: false
    }
}

export default apiHandler({
    get: getFurnitureByRoomId,
    post: createFurnitureForRoom
})

function getFurnitureByRoomId(req, res) {
    const roomId = req.query.id
    return roomFurnitureUtils.getFurnitureByRoomId(roomId).then(furnitures => {
        return res.status(200).json(furnitures)
    })
}

function createFurnitureForRoom(req, res) {
    const roomId = req.query.id
    const furniture = req.body
    return roomFurnitureUtils.createFurnitureForRoom(roomId, furniture).then(() => {
        return res.status(200).json({})
    })
}