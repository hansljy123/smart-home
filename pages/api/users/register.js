import bcrypt from 'bcryptjs'
import getConfig from 'next/config'
import { apiHandler } from '../../../helpers/api/api-handler'
import { databaseUtils } from '../../../database/database'

const { serverRuntimeConfig } = getConfig()

export default apiHandler({
    post: register
})

export const config = {
    api: {
        bodyParser: false
    }
}

async function register(req, res) {
    const {password, ...user} = req.body

    return databaseUtils.users.getUserByPhoneNumber(user.phoneNumber)
    .then(userInDatabase => {
        if (userInDatabase !== undefined && userInDatabase.length != 0) {
            throw `手机号 ${user.phoneNumber} 已被注册`
        } else {
            return Promise.resolve()
        }
    })
    .then(() => {
        return databaseUtils.users.getUserByName(user.userName).then(userInDatabase => {
            if (userInDatabase !== undefined && userInDatabase.length != 0) {
                throw `用户名 ${user.userName} 已存在`
            }
            user.hash = bcrypt.hashSync(password, serverRuntimeConfig.encryptionConfig.salt)
            return user
        }).then(user => {
            return databaseUtils.users.insertUser(user)
        }).then(userId => {
            res.status(200).json({
                userId
            })
        })
    })

}