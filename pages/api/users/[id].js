import { apiHandler } from "../../../helpers/api/api-handler"
import { databaseUtils } from "../../../database/database"
import { omit } from "../../../helpers/api/omit"
import getConfig from "next/config"
import bcrypt from 'bcryptjs'

const { serverRuntimeConfig } = getConfig()

const userUtils = databaseUtils.users

export const config = {
    api: {
        bodyParser: false
    }
}

export default apiHandler({
    get: getById,
    put: updateInfo,
    delete: _delete
})

function getById(req, res) {
    const id = req.query.id
    return userUtils.getUserById(id).then(user => {
        if (!user) {
            throw 'User not found'
        }
        return res.status(200).json(omit(user, 'hash'))
    })
}

function updateInfo(req, res) {
    const id = req.query.id
    const { oldPassword, newPassword, ...params } = req.body
    return userUtils.getUserById(id).then(async user => {
        if (!user) {
            throw 'User not found'
        }

        if (!bcrypt.compareSync(oldPassword, user.hash)) {
            throw 'Wrong password'
        }

        if (params.userName && user.userName !== params.userName) {
            await userUtils.getUserByName(params.userName).then(user => {
                if (user) {
                    throw `Username ${params.userName} already exists`
                }
            })
        }
        user = {
            hash: bcrypt.hashSync(newPassword, serverRuntimeConfig.encryptionConfig.salt),
            ...params
        }
        return userUtils.updateUserById(id, user)
    }).then(() => {
        return res.status(200).json({})
    })
}

function _delete(req, res) {
    userUtils.deleteUserById(req.query.id)
    return res.status(200).json({})
}