import jwt from 'jsonwebtoken'
import getConfig from 'next/config'
import { apiHandler } from '../../../helpers/api/api-handler'
import { databaseUtils } from '../../../database/database'
import bcrypt from 'bcryptjs'

export default apiHandler({
    post: authentication 
})

export const config = {
    api: {
        bodyParser: false
    }
}

const {serverRuntimeConfig} = getConfig()

function authentication(req, res) {
    const {userName, password} = req.body

    return databaseUtils.users.getUserByName(userName).then(user => {
        if (!(user && bcrypt.compareSync(password, user.hash))) {
            throw '用户名和密码不匹配'
        }

        const token = jwt.sign(
            { sub: user.userId },
            serverRuntimeConfig.encryptionConfig.secret,
            { expiresIn: '1d' }
        )

        return res.status(200).json({
            userId: user.userId,
            userName: user.userName,
            token
        })
    })
}