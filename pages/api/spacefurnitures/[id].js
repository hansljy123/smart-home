import { apiHandler } from "../../../helpers/api/api-handler"
import { databaseUtils } from "../../../database/database"

export default apiHandler({
    get: getFurniturePositions
})

function getFurniturePositions(req, res) {
    const spaceId = req.query.id

    return databaseUtils.spaceFurniture.getFurniturePositions(spaceId)
    .then(positions => {
        return res.status(200).json(positions)
    })
}