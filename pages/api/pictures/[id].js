import { resolve } from "path";
import { databaseUtils } from "../../../database/database"
import { apiHandler } from "../../../helpers/api/api-handler";

export default apiHandler({
    get: ImageServer
})

export const config = {
    api: {
        bodyParser: false
    }
}

function ImageServer(req, res) {
    const pictureId = req.query.id
    return databaseUtils.picture.getPicture(pictureId).then(async data => {
        res.setHeader('Content-Type', 'image/jpeg');
        return res.send(data);
    })
}