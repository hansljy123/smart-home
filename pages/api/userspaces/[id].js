import { apiHandler } from '../../../helpers/api/api-handler'
import { databaseUtils } from '../../../database/database'

const userSpaceUtils = databaseUtils.userSpace

export default apiHandler({
    get: getUserSpacesById,
    post: createUserSpace
})

export const config = {
    api: {
        bodyParser: false,
    },
  };

function getUserSpacesById(req, res) {
    const userId = req.query.id
    return userSpaceUtils.getUserSpaceById(userId).then((spaces) => {
        return res.status(200).json(spaces)
    })
}
function createUserSpace(req, res) {
    const userId = req.query.id
    const space = req.body
    return userSpaceUtils.createUserSpace(userId, space).then(() => {
        return res.status(200).json({})
    })
}