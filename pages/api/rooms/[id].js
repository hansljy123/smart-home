import { apiHandler } from "../../../helpers/api/api-handler"
import { databaseUtils } from "../../../database/database"

const roomUtils = databaseUtils.room

export const config = {
    api: {
        bodyParser: false
    }
}

export default apiHandler({
    get: getRoom,
    delete: deleteRoom,
    put: updateRoom
})

function getRoom(req, res) {
    const roomId = req.query.id

    return roomUtils.getRoom(roomId).then(room => {
        return res.status(200).json(room)
    })
}

function deleteRoom(req, res) {
    const roomId = req.query.id

    return roomUtils.deleteRoom(roomId).then(() => {
        return res.status(200).json({})
    })
}

function updateRoom(req, res) {
    const roomId = req.query.id
    const {spaceId, ...room} = req.body

    return roomUtils.updateRoom(spaceId, roomId, room).then(() => {
        return res.status(200).json({})
    })
}