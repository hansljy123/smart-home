import Layout from "../components/Layout"
import { Button, Card, CardContent, Grid, Paper, TextField, Typography } from "@mui/material"
import React, { useState } from "react"
import { Box } from "@mui/system"
import * as yup from 'yup'
import { userService } from "../services/user.service"
import { useRouter } from "next/router"
import CenterBox from "../components/CenterBox"
import InputPanel from "../components/InputPanal"
import Link from "next/link"

export default function Login() {
    const router = useRouter()
    const schema = yup.object().shape({
        userName: yup.string().required('请输入用户名'),
        password: yup.string().required('请输入密码')
    })

    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')

    function handleLogin() {
        let loginInfo = {
            userName: userName,
            password: password
        }
        schema.validate(loginInfo).then(() => {
            return userService.login(userName, password)
        }).then(() => {
            return router.push('/')
        })
    }

    return (
        <Layout>
            <CenterBox>
                <InputPanel title='用户登陆' items={[
                    {
                        name: '用户名',
                        input: (
                            <TextField variant="outlined" defaultValue='' onChange={event => {
                                setUserName(event.target.value)
                            }} sx={{marginLeft: 2}} fullWidth></TextField>
                        )
                    }, {
                        name: '密码',
                        input: (
                            <TextField variant="outlined" type='password' defaultValue='' onChange={event => {
                                setPassword(event.target.value)
                            }} sx={{marginLeft: 2}} fullWidth></TextField>
                        )
                    }
                ]} buttons={
                    <>
                        <Link href='/registration'>
                            <Button variant="contained">
                                注册
                            </Button>
                        </Link>
                        <Button variant="contained" onClick={handleLogin} sx={{marginLeft: 2}}>登陆</Button>
                    </>
                }>
                </InputPanel>
            </CenterBox>
        </Layout>
    )
}