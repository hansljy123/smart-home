import { Typography, Card, CardContent, Box, Grid, TextField, IconButton, Icon, Modal, Button, CardActionArea, Link} from "@mui/material";
import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout";
import Image from "next/image";
import SearchIcon from '@mui/icons-material/Search';
import { userSpaceService } from "../../services/userspace.service";
import { userService } from "../../services/user.service";
import AddIcon from '@mui/icons-material/Add';
import ModalWrapper from "../../components/ModalWrapper";
import InputPanel from "../../components/InputPanal";
import { Router, useRouter, withRouter } from "next/router";
import { default as NextLink } from "next/link";
import { spaceService } from '../../services/space.service'
import CenterBox from "../../components/CenterBox";
import * as yup from 'yup'

export default function UserSpace(props) {
    const [spaces, setSpaces] = useState([])
    const [newSpaceName, setNewSpaceName] = useState('')
    const [newSpaceDescription, setNewSpaceDescription] = useState('')
    const [updateSpaceName, setUpdateSpaceName] = useState('')
    const [updateDescription, setUpdateDescription] = useState('')

    const [newFile, setNewFile] = useState(null)

    const router = useRouter()

    const schema = yup.object().shape({
        spaceName: yup.string().required('请输入场景名'),
        description: yup.string().required('请输入密码')
    })


    useEffect(() => {
        const queryInfo = router.query
        const userId = queryInfo.id
        if (userId) {
            userSpaceService.getUserAllSpaces(userId).then((spaces) => {
                setSpaces(spaces)
            })
        }
    }, [router.isReady])

    function refresh() {
        const userId = router.query.id
        return userSpaceService.getUserAllSpaces(userId).then((spaces) => {
            setSpaces(spaces)
        })
    }


    async function handleNewUserSpace() {
        let userId = userService.localUserValue().userId

        let valid = true
        try {
            await schema.validate({
                spaceName: newSpaceName,
                description: newSpaceDescription
            })
        } catch (err) {
            alert(err.errors[0]);
            valid = false
        }

        if (valid) {
            const newSpace = new FormData()
            newSpace.append('spaceName', newSpaceName)
            newSpace.append('description', newSpaceDescription)
            newSpace.append('picture', newFile)
            return userSpaceService.createUserSpace(userId, newSpace)
        } else {
            return Promise.resolve()
        }
    }

    function handleDeleteSpace(spaceId) {
        return spaceService.deleteSpace(spaceId)
    }

    async function handleUpdateSpace(spaceId) {
        const userId = router.query.id

        let valid = true
        try {
            await schema.validate({
                spaceName: updateSpaceName,
                description: updateDescription
            })
        } catch (err) {
            alert(err.errors[0]);
            valid = false
        }

        if (valid) {
            return spaceService.updateSpace(spaceId, {
                userId,
                spaceName: updateSpaceName,
                description: updateDescription
            })
        } else {
            return Promise.resolve()
        }

    }

    return (
        <Layout>
            <Grid container direction="row" spacing={0}>
                <Grid item>
                    <Typography variant="h3" marginY={2} align="center">用户场景</Typography>
                </Grid>
            </Grid>
            <Grid container direction="row" spacing={0.5}>
                {
                    spaces.map((space, id) => {
                        return (
                            <Grid key={space.spaceId} container item xs={12} md={6} lg={4} sx={{padding: 0.5}}> 
                                <Card sx={{width: "100%", height: 416}} elevation={5}>
                                    <CardContent sx={{height: '100%'}}>
                                        <Link component={NextLink} href={`/spaces/${space.spaceId}`}>
                                            <Typography display='inline' variant="h4">{space.spaceName}</Typography>
                                        </Link>
                                        <Typography display='inline'>{space.description}</Typography>
                                        <br></br>
                                        <Box sx={{width: '100%', height: '80%', position: 'relative'}}>
                                            <Image src={`/api/pictures/${space.pictureId}`} alt={space.spaceName} objectFit='contain' fill></Image>
                                        </Box>
                                        <Box display='flex' justifyContent='flex-end' marginY={1}>
                                            <ModalWrapper button={
                                                <Button variant="contained">修改</Button>
                                            } handleClick={() => {
                                                setUpdateDescription(space.description)
                                                setUpdateSpaceName(space.spaceName)
                                            }}>
                                                <CenterBox>
                                                    <InputPanel title='修改用户场景' items={[
                                                        {
                                                            name: '场景名',
                                                            input: (
                                                                <TextField variant="outlined" defaultValue={space.spaceName} onChange={(event) => {
                                                                    setUpdateSpaceName(event.target.value)
                                                                }} sx={{marginLeft: 2}} fullWidth>
                                                                </TextField>
                                                            )
                                                        }, {
                                                            name: '描述',
                                                            input: (
                                                                <TextField variant="outlined" defaultValue={space.description} onChange={(event) => {
                                                                    setUpdateDescription(event.target.value)
                                                                }} sx={{marginLeft: 2}} fullWidth>
                                                                </TextField>
                                                            )
                                                        }
                                                    ]} buttons={<Button onClick={async () => {
                                                        await handleUpdateSpace(space.spaceId)
                                                        await refresh()
                                                    }}>提交</Button>}></InputPanel>
                                                </CenterBox>
                                            </ModalWrapper>
                                            <Button variant="contained" color="warning" sx={{marginLeft: 1}} onClick={async () => {
                                                await handleDeleteSpace(space.spaceId)
                                                await refresh()
                                            }}>删除</Button>
                                        </Box>
                                    </CardContent>
                                </Card>
                            </Grid>
                        )
                    })
                }
                <Grid container item xs={12} md={6} lg={4} sx={{padding: 0.5}}>
                    <Card sx={{width: "100%", height: 416}} elevation={5}>
                        <CardContent sx={{display: 'flex', width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
                            <ModalWrapper button={
                                <IconButton>
                                    <AddIcon fontSize="large"></AddIcon>
                                </IconButton>
                            }>
                                <CenterBox>
                                    <InputPanel title='新建用户场景' items={[
                                        {
                                            name: '场景名',
                                            input: (
                                                <TextField variant="outlined" defaultValue='' onChange={(event) => {
                                                    setNewSpaceName(event.target.value)
                                                }} sx={{marginLeft: 2}} fullWidth>
                                                </TextField>
                                            )
                                        }, {
                                            name: '描述',
                                            input: (
                                                <TextField variant="outlined" defaultValue='' onChange={(event) => {
                                                    setNewSpaceDescription(event.target.value)
                                                }} sx={{marginLeft: 2}} fullWidth>
                                                </TextField>
                                            )
                                        }, {
                                            name: '场景图',
                                            input: (
                                                <Box>
                                                    <Button sx={{marginLeft: 2}} variant="contained" component="label">
                                                        Upload
                                                        <input hidden accept="image/*" type="file" onChange={event => {
                                                            if (event.target.files) {
                                                                setNewFile(event.target.files[0])
                                                            }
                                                        }}/>
                                                    </Button>
                                                    <Typography marginLeft={2} display='inline' whiteSpace='nowrap'>{newFile && `${newFile.name}`}</Typography>
                                                </Box>
                                            )
                                        }
                                    ]} buttons={<Button onClick={async () => {
                                        await handleNewUserSpace()
                                        await refresh()
                                    }}>提交</Button>}></InputPanel>
                                </CenterBox>
                            </ModalWrapper>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </Layout>
    )
}