/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  serverRuntimeConfig: {
    encryptionConfig: {
      secret: 'random-string', // change it into some real random string
      salt: 10,
    },
    databaseConfig: {
      host: 'localhost',
      user: 'hans',
      password: 'ljy123456',
      database: 'SmartHome'
    }
  },
  publicRuntimeConfig: {
    apiUrl: process.env.NODE_ENV === 'development'
      ? 'http://localhost:3000/api'
      : 'http://localhost:3000/api'
  }
}

module.exports = nextConfig
